# Prototyping board for the Explorer 16/32

Proto board for use with the Microchip Explorer 16/32.
Board is screened with pin ID for the *PIC32MZ2048ECH100* PIM.

The *TFTsocket* branch includes pads for a SainSmart TFT.
